# dmenu with minor alterations

Just a fork of dmenu with some slight config changes so my scripts can access
it

## Installation

After making any config changes, `make`, `sudo make install`
